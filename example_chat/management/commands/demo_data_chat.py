# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from chat.models import Channel


class Command(BaseCommand):

    help = "Chat Demo Data"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        Channel.objects.init_channel(
            Channel.ACTIVITY, "https://chat.hatherleigh.info"
        )
        self.stdout.write("Complete: {}".format(self.help))
