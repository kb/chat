# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.urls import reverse

from login.tests.fixture import perm_check
from chat.models import Channel
from chat.tests.factories import ChannelFactory


@pytest.mark.django_db
def test_channel_list(perm_check):
    url = reverse("chat.channel.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_channel_update(perm_check):
    channel = ChannelFactory()
    url = reverse("chat.channel.update", args=[channel.pk])
    perm_check.staff(url)
