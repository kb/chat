# -*- encoding: utf-8 -*-
import pytest
import responses

from django.utils import timezone
from http import HTTPStatus

from chat.models import Channel
from chat.tasks import send_chat_messages
from chat.tests.factories import ChannelFactory, MessageFactory


@pytest.mark.django_db
@responses.activate
def test_send_chat_messages():
    responses.add(
        responses.POST,
        "https://chat.hatherleigh.info",
        json={"Status": "OK"},
        status=HTTPStatus.OK,
    )
    channel = ChannelFactory(slug=Channel.ACTIVITY)
    MessageFactory(channel=channel, title="a", retries=3)
    MessageFactory(channel=channel, title="b", completed_date=timezone.now())
    MessageFactory(channel=channel, title="c")
    MessageFactory(channel=channel, title="d", retries=99)
    assert 2 == send_chat_messages()
