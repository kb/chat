# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_send_chat_messages():
    call_command("send_chat_messages")
