# -*- encoding: utf-8 -*-
import pytest
import responses

from datetime import date
from django.utils import timezone
from freezegun import freeze_time
from http import HTTPStatus

from chat.models import Channel, Message
from chat.tests.factories import ChannelFactory, MessageFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_message():
    with freeze_time("2017-05-21 23:55:01"):
        message = Message.objects.create_message(
            ChannelFactory(slug=Channel.ACTIVITY),
            UserFactory(username="patrick"),
            "Apple",
            "Fruit",
        )
    assert date(2017, 5, 21) == message.created.date()
    assert "patrick" == message.user.username
    assert "Apple" == message.title
    assert "Fruit" == message.description


@pytest.mark.django_db
def test_create_message_no_description():
    with freeze_time("2017-05-21 23:55:01"):
        message = Message.objects.create_message(
            ChannelFactory(slug=Channel.ACTIVITY),
            UserFactory(username="patrick"),
            "Apple",
        )
    assert date(2017, 5, 21) == message.created.date()
    assert "patrick" == message.user.username
    assert "Apple" == message.title
    assert "" == message.description


@pytest.mark.django_db
def test_create_message_title_too_long():
    title = (
        "A 1234567 B 1234567 C 1234567 D 1234567 E 1234567 "
        "F 1234567 G 1234567 H 1234567 I 1234567 J 1234567 "
        "K 1234567 J 1234567 K 1234567 L 1234567 M 1234567 "
        "N 1234567 O 1234567 P 1234567 Q 1234567 R 1234567 "
        "S 1234567 T 1234567 U 1234567 V 1234567 W 1234567 "
    )
    message = Message.objects.create_message(
        ChannelFactory(), UserFactory(), title
    )
    assert (
        "A 1234567 B 1234567 C 1234567 D 1234567 E 1234567 "
        "F 1234567 G 1234567 H 1234567 I 1234567 J 1234567 "
        "K 1234567 J 1234567 K 1234567 L 1234567 M 1234567 "
        "N 1234567 O 1234567 P 1234567 Q 1234567 R 1234 ..."
    ) == message.title


@pytest.mark.django_db
def test_current():
    MessageFactory(title="a")
    MessageFactory(title="b")
    assert ["b", "a"] == [x.title for x in Message.objects.current()]


@pytest.mark.django_db
def test_ordering():
    MessageFactory(title="a")
    MessageFactory(title="b")
    assert ["b", "a"] == [x.title for x in Message.objects.all()]


@pytest.mark.django_db
@responses.activate
def test_process():
    responses.add(
        responses.POST,
        "https://chat.hatherleigh.info",
        json={"Status": "OK"},
        status=HTTPStatus.OK,
    )
    channel = ChannelFactory(slug=Channel.ACTIVITY)
    message_1 = MessageFactory(channel=channel, title="a", retries=3)
    MessageFactory(channel=channel, title="b", completed_date=timezone.now())
    message_2 = MessageFactory(channel=channel, title="c")
    MessageFactory(channel=channel, title="d", retries=99)
    assert [message_1.pk, message_2.pk] == Message.objects.process()


@pytest.mark.django_db
def test_str():
    message = MessageFactory(
        channel=ChannelFactory(slug=Channel.ACTIVITY),
        title="Patrick",
        retries=3,
    )
    assert "Patrick [activity] (retry x 3)" == str(message)
