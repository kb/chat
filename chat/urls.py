# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import ChannelListView, ChannelUpdateView


urlpatterns = [
    re_path(
        r"^channel/$", view=ChannelListView.as_view(), name="chat.channel.list"
    ),
    re_path(
        r"^channel/(?P<pk>\d+)/update/$",
        view=ChannelUpdateView.as_view(),
        name="chat.channel.update",
    ),
]
