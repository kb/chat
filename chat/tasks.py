# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from .models import Message


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def send_chat_messages():
    logger.info("Send chat messages...")
    result = Message.objects.process()
    logger.info("Sent {} chat messages...".format(len(result)))
    return len(result)
