# -*- encoding: utf-8 -*-
import logging
import requests

from django.conf import settings
from django.db import models
from http import HTTPStatus

from base.model_utils import (
    RetryModel,
    RetryModelManager,
    TimedCreateModifyDeleteModel,
)


logger = logging.getLogger(__name__)


class ChannelManager(models.Manager):
    def _create_channel(self, slug, url=None):
        if url is None:
            x = self.model(slug=slug)
        else:
            x = self.model(slug=slug, url=url)
        x.save()
        return x

    def current(self):
        return self.model.objects.all().exclude(deleted=True)

    def init_channel(self, slug, url=None):
        try:
            x = self.model.objects.get(slug=slug)
            if url is not None:
                x.url = url
            x.save()
        except self.model.DoesNotExist:
            x = self._create_channel(slug, url)
        return x


class Channel(TimedCreateModifyDeleteModel):

    ACTIVITY = "activity"

    slug = models.SlugField(max_length=100, unique=True)
    url = models.URLField(max_length=200)

    objects = ChannelManager()

    class Meta:
        ordering = ("slug", "url")

    def __str__(self):
        return "{}: {}".format(self.slug, self.url)


class MessageManager(RetryModelManager):
    def create_message(self, channel, user, title, description=None):
        if description is None:
            description = ""
        if len(title) > 200:
            title = "{} ...".format(title[:196])
        x = self.model(
            channel=channel,
            user=user,
            title=title,
            description=description,
            max_retry_count=self.model.DEFAULT_MAX_RETRY_COUNT,
        )
        x.save()
        return x

    def current(self):
        return self.model.objects.all()


class Message(RetryModel):
    """Chat message waiting to be sent."""

    DEFAULT_MAX_RETRY_COUNT = 10

    created = models.DateTimeField(auto_now_add=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    objects = MessageManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Chat Message"

    def __str__(self):
        result = "{} [{}]".format(self.title, self.channel.slug)
        retry_str = self.retry_str()
        if retry_str:
            result = "{} ({})".format(result, retry_str)
        return result

    def process(self):
        """Process the message.

        .. note:: This method is running inside a transaction.

        This method is called by ``RetryModelManager``
        (see ``base/model_utils.py``).

        """
        result = False
        text = self.title
        if self.description:
            text = "{}\n{}".format(text, self.description)
        response = requests.post(
            self.channel.url,
            json={
                "channel": self.channel.slug,
                "username": self.user.username,
                "text": text,
            },
        )
        if HTTPStatus.OK == response.status_code:
            result = True
        else:
            logger.error(
                "Cannot post message {} to Mattermost, "
                "{}: {}".format(self.pk, response.status_code, str(response))
            )
        return result
