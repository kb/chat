# Generated by Django 2.2.10 on 2020-02-10 10:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("chat", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="message",
            name="fail_date",
            field=models.DateTimeField(blank=True, null=True),
        )
    ]
