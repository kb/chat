# -*- encoding: utf-8 -*-
from base.form_utils import RequiredFieldForm

from .models import Channel


class ChannelForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["url"]
        f.widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Channel
        fields = ["url"]
