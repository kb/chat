# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.urls import reverse
from django.views.generic import ListView, UpdateView

from base.view_utils import BaseMixin
from chat.forms import ChannelForm
from .models import Channel


class ChannelListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    def get_queryset(self):
        return Channel.objects.current()


class ChannelUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ChannelForm
    model = Channel

    def get_success_url(self):
        return reverse("chat.channel.list")
